require 'yaml'
require 'gitlab'

GROUP_NAME = 'gitlab-org'
TARGET_PROJECTS = %w[gitlab-org/gitlab-ce gitlab-org/gitlab-ee gitlab-org/gitlab-runner]
MILESTONES = %w[9.0 9.1 9.2 9.3 9.4 9.5 10.0 10.1 10.2]

secret = YAML.load_file('secret.yml')

Gitlab.configure do |config|
  config.endpoint       = 'https://gitlab.com/api/v4' # API endpoint URL, default: ENV['GITLAB_API_ENDPOINT']
  config.private_token  = secret['GITLAB_API_PRIVATE_TOKEN']      # user's private token or OAuth2 access token, default: ENV['GITLAB_API_PRIVATE_TOKEN']
  # Optional
  # config.user_agent   = 'Custom User Agent'          # user agent, default: 'Gitlab Ruby Gem [version]'
  # config.sudo         = 'user'                       # username for sudo mode, default: nil
end

# def closed_issues_count(milestone, id_name)
#   if id_name == 'gitlab-org/gitlab-runner'
#     Gitlab.issues(id_name, milestone: milestone, state: 'closed').auto_paginate.count
#   else
#     Gitlab.issues(id_name, labels: 'CI/CD', milestone: milestone, state: 'closed').auto_paginate.count
#   end
# end

# def merged_merge_requests_count(milestone, id_name)
#   if id_name == 'gitlab-org/gitlab-runner'
#     Gitlab.merge_requests(id_name, milestone: milestone, state: 'merged').auto_paginate.count
#   else
#     Gitlab.merge_requests(id_name, labels: 'CI/CD', milestone: milestone, state: 'merged').auto_paginate.count
#   end
# end

def closed_issues_count(milestone, id_name)
  params = if id_name == 'gitlab-org/gitlab-runner'
             { milestone: milestone, state: 'closed' }
           else
             { milestone: milestone, state: 'closed', labels: 'CI/CD' }
           end

  user_notes_count = 0  # Discussion Intensitivity
  closed_count = 0

  Gitlab.issues(id_name, **params).auto_paginate do |issue|
    user_notes_count = user_notes_count + issue.user_notes_count.to_i
    closed_count = closed_count + 1
  end

  return user_notes_count, closed_count
end

def merged_merge_requests_count(milestone, id_name)
  params = if id_name == 'gitlab-org/gitlab-runner'
             { milestone: milestone, state: 'merged' }
           else
             { milestone: milestone, state: 'merged', labels: 'CI/CD' }
           end

  changes_count = 0 # File changes count
  user_notes_count = 0  # Discussion Intensity
  merged_count = 0

  Gitlab.merge_requests(id_name, **params).auto_paginate do |merge_request|
    changes_count = changes_count + merge_request.changes_count.to_i
    user_notes_count = user_notes_count + merge_request.user_notes_count.to_i
    merged_count = merged_count + 1
  end

  return changes_count, user_notes_count, merged_count
end

puts "Milestone, Closed Issue Count, Merged Merge Requests Count, Discussion Intensity (Total user notes count), File change count"

MILESTONES.each do |milestone|
  # Closed Issues
  # closed_issues_count1 = TARGET_PROJECTS.inject(0) do |sum, id_name|
  #   sum + closed_issues_count(milestone, id_name)
  # end

  issues_metrics = TARGET_PROJECTS.inject({}) do |result, id_name|
    result[:user_notes_count] ||= 0
    result[:closed_count] ||= 0
    user_notes_count, closed_count = closed_issues_count(milestone, id_name)
    result[:user_notes_count] = result[:user_notes_count] + user_notes_count
    result[:closed_count] = result[:closed_count] + closed_count
    result
  end

  # Merged Merge request
  merge_requests_metrics = TARGET_PROJECTS.inject({}) do |result, id_name|
    result[:changes_count] ||= 0
    result[:user_notes_count] ||= 0
    result[:merged_count] ||= 0
    changes_count, user_notes_count, merged_count = merged_merge_requests_count(milestone, id_name)
    result[:changes_count] = result[:changes_count] + changes_count
    result[:user_notes_count] = result[:user_notes_count] + user_notes_count
    result[:merged_count] = result[:merged_count] + merged_count
    result
  end

  puts "#{milestone}, #{issues_metrics[:closed_count]}, #{merge_requests_metrics[:merged_count]}, #{issues_metrics[:user_notes_count] + merge_requests_metrics[:user_notes_count]}, #{merge_requests_metrics[:changes_count]}"
end

# TARGET_PROJECTS.map do |id_name|
#   Gitlab.issues(id_name, labels: 'Deliverable,CI/CD', milestone: '10.3', state: 'closed')
# end

# # Get CI/CD related issues
# issues = Gitlab.group_issues(group_gitlab_id, { labels: 'CI/CD' })

# puts issues.inspect

# Closed issues per Feature Freezing day
# Team memebers Activity

# TODO: Line number changes count
# TODO: GUI Automation


# group_gitlab = Gitlab.group_search(GROUP_NAME).first
# group_gitlab_id = group_gitlab.id

# puts group_gitlab.inspect
# puts "Group ID is #{group_gitlab_id}"

# group_projects = Gitlab.group_projects(group_gitlab_id, search: 'Community')  
# puts "group_projects: #{group_projects.inspect}"

# project_ids = TARGET_PROJECTS.map do |id_name|
#   Gitlab.project(id_name).id
# end